//
//  PlayerViewController.swift
//  songlist
//
//  Created by Alex Tsyss on 10.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import UIKit

extension PlayerState {
    var bigIconName: String {
        switch self {
        case .started( _):
            return "player-pause"
        case .finished( _):
            return "player-play"
        }
    }
}

class PlayerViewController: UIViewController {
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var progressSlider: UICustomSlider!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    var songs: [Song] = []
    var player: Player!
    
    private lazy var timeFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        formatter.unitsStyle = .positional
        return formatter
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressLabel.text = " "
        durationLabel.text = " "

        setupNavigatioBar()
        setupBlurView()
        
        player.addObserver { [weak self] state in
            self?.updateUiForCurrentItem(state: state)
        }
        
        player.addProgressObserver { [weak self] (progress, duration) in
            self?.updateItemProgress(progress: progress, duration: duration)
        }
        
        updateUiForCurrentItem(state: .started(player.currentItemIndex))
    }

    // MARK: - UIControls
    @IBAction func dismissButtonPressed(_ sender: Any) {
        dismiss()
    }
    
    @IBAction func playButtonPressed() {
        player.togglePlaying()
    }
    
    @IBAction func nextButtonPressed() {
        player.next()
    }
    
    @IBAction func prevButtonPressed() {
        player.prev()
    }
    
    @IBAction func progressChangedByUser(_ sender: UICustomSlider) {
        player.seekTo(position: sender.value)
    }
    
    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
        dismiss()
    }
    
    // MARK: - Helpers
    private func dismiss() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    private func updateUiForCurrentItem(state: PlayerState) {
        let song = songs[player.currentItemIndex]
        
        itemNameLabel.text = song.name
        ImageDownloader.shared.getImage(url: song.picture.url) { [weak self] image in
            self?.backgroundImageView.image = image
            self?.logoImageView.image = image
        }
        
        playButton.setImage(UIImage(named: state.bigIconName), for: .normal)
    }
    
    private func updateItemProgress(progress: Double, duration: Double) {
        progressLabel.text = timeFormatter.string(from: progress)
        durationLabel.text = timeFormatter.string(from: duration)
        
        if progressSlider.isTouching == false {
            progressSlider.value = Float(progress / duration)
        }
    }
    
    private func setupBlurView() {
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.insertSubview(blurEffectView, at: 1)
    }
    
    private func setupNavigatioBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
}

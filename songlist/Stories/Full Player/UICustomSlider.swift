//
//  UICustomSlider.swift
//  songlist
//
//  Created by Alexey Tsyss on 13/10/2017.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import UIKit

class UICustomSlider: UISlider {
    
    var isTouching = false

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouching = true
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouching = false
        super.touchesEnded(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouching = false
        super.touchesCancelled(touches, with: event)
    }
}

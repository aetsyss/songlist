//
//  SongTableViewCell.swift
//  songlist
//
//  Created by Alexey Tsyss on 07/10/2017.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {

    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var songCoverImageView: UIImageView!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    
    var playCallback: (() -> Void)?
    
    var isPlaying = false {
        didSet {
            let playButtonImageName = isPlaying ? "pause" : "play"
            playButton.setImage(UIImage(named: playButtonImageName), for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        authorImageView.layer.masksToBounds = false
        authorImageView.layer.cornerRadius = authorImageView.frame.height/2
        authorImageView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillWith(song: Song, isPlaying: Bool, playCallback: (() -> Void)?) {
        songNameLabel.text = song.name
        authorNameLabel.text = song.author.name
        
        self.isPlaying = isPlaying
        
        self.playCallback = playCallback
        
        songCoverImageView.image = nil
        ImageDownloader.shared.getImage(url: song.picture.url) { [weak self] image in
            self?.songCoverImageView.image = image
        }

        authorImageView.image = nil
        ImageDownloader.shared.getImage(url: song.author.picture.url) { [weak self] image in
            self?.authorImageView.image = image
        }
    }
    
    @IBAction func playButtonPressed() {
        playCallback?()
    }
}

//
//  SongsListViewController.swift
//  songlist
//
//  Created by Alex Tsyss on 07.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import UIKit
import AVKit

extension PlayerState {
    var smallIconName: String {
        switch self {
        case .started( _):
            return "pause"
        case .finished( _):
            return "play"
        }
    }
}

class SongsListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomPlayerPositionConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomPlayerView: UIView!
    @IBOutlet weak var bottomPlayerItemLabel: UILabel!
    @IBOutlet weak var bottomPlayerPlayButton: UIButton!
    
    private let songsUrl = URL(string: "https://gist.githubusercontent.com/anonymous/fec47e2418986b7bdb630a1772232f7d/raw/5e3e6f4dc0b94906dca8de415c585b01069af3f7/57eb7cc5e4b0bcac9f7581c8.json")!
    
    private let bottomPlayerSegueId = "player"
    
    var songsDownloader: SongsDownloader!
    var songs: [Song] = [] {
        didSet {
            player = Player(items: songs)
            player.addObserver { [weak self] state in
                self?.bottomPlayerHandlePlayerState(state: state)
                self?.tableViewHandlePlayerState(state: state)
            }
            
            self.tableView.reloadData()
        }
    }
    
    var player: Player!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        
        bottomPlayerInit()

        songsDownloader = SongsDownloader(endpointUrl: songsUrl)
        songsDownloader.download { [weak self] result in
            switch result {
            case .success(let songs):
                self?.songs = songs
            case .failure(let error):
                if let error = error {
                    self?.presentSimpleAlert(with: error.localizedDescription)
                } else {
                    self?.presentSimpleAlert(with: ":(( Failed to download data. Try again later...")
                }
            }
        }
    }
    
    // MARK: - UIControls
    @IBAction func playerTapped(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: bottomPlayerSegueId, sender: nil)
    }
    
    @IBAction func playButtonPressed() {
        player.togglePlaying()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else {
            return
        }
        
        if id == bottomPlayerSegueId {
            guard let nav = segue.destination as? UINavigationController,
                let vc = nav.topViewController as? PlayerViewController else {
                    return
            }
            
            vc.songs = songs
            vc.player = player
        }
    }
    
    // MARK: - TableView Helpers
    private func tableViewHandlePlayerState(state: PlayerState) {
        var indexPath: IndexPath!
        var isPlaying: Bool!
        
        switch state {
        case .started(let index):
            indexPath = IndexPath(row: index, section: 0)
            isPlaying = true
        case .finished(let index):
            indexPath = IndexPath(row: index, section: 0)
            isPlaying = false
        }
        
        guard let cell = tableView.cellForRow(at: indexPath) as? SongTableViewCell else {
            return
        }
        
        cell.isPlaying = isPlaying
    }
    
    // MARK: - Bottom Player
    private func bottomPlayerInit() {
        bottomPlayerView.dropShadow()
        bottomPlayerHide(animated: false)
    }
    
    private func bottomPlayerHandlePlayerState(state: PlayerState) {
        bottomPlayerPlayButton.setImage(UIImage(named: state.smallIconName), for: .normal)
        
        switch state {
        case .started(let index):
            bottomPlayerItemLabel.text = songs[index].name
            bottomPlayerShow()
            
        case .finished( _):
            bottomPlayerHide()
        }
    }
    
    private func bottomPlayerShow() {
        UIView.animate(withDuration: 0.5) {
            self.bottomPlayerPositionConstraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    private func bottomPlayerHide(animated: Bool = true) {
        if animated == false {
            bottomPlayerPositionConstraint.constant = bottomPlayerView.bounds.size.height
        } else {
            UIView.animate(withDuration: 0.5) {
                self.bottomPlayerPositionConstraint.constant = self.bottomPlayerView.bounds.size.height
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - Extensions
extension SongsListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "song", for: indexPath) as! SongTableViewCell
        
        let isActiveTrack = player.currentItemIndex == indexPath.row
        
        let song = songs[indexPath.row]
        cell.fillWith(song: song, isPlaying: (isActiveTrack && player.queuePlayer.isPlaying)) { [weak self] in
            guard let strongSelf = self else { return }
            
            if strongSelf.player.currentItemIndex == indexPath.row {
                strongSelf.player.togglePlaying()
            } else {
                strongSelf.player.playFrom(index: indexPath.row)
            }
        }
        
        return cell
    }
}

extension SongsListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}

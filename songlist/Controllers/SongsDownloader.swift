//
//  SongsDownloader.swift
//  songlist
//
//  Created by Alex Tsyss on 07.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//
//  Use this class to download the songs data from the backend and parses it to the data model.
//  Pass the backend end point URL via init method.
//  It uses the HttpClientProtocol object to request the backend.
//  The HttpClientProtocol instance can be passed via the init method.

import Foundation

class SongsDownloader {
    typealias SongsDownloaderCompletion = (SongsDownloaderResult) -> Void
    
    private var httpClient: HttpClientProtocol!
    private var endpointUrl: URL!
    
    init(httpClient: HttpClientProtocol = HttpClient(), endpointUrl: URL) {
        self.httpClient = httpClient
        self.endpointUrl = endpointUrl
    }
    
    func download(completion: SongsDownloaderCompletion?) {
        httpClient.request(url: endpointUrl) { (result) in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                
                do {
                    let songList = try decoder.decode(SongList.self, from: data)
                    completion?(SongsDownloaderResult.success(songList.songs))
                } catch let error {
                    completion?(SongsDownloaderResult.failure(error))
                }
                
            case .failure(let error):
                completion?(SongsDownloaderResult.failure(error))
            }
        }
    }
}

extension SongsDownloader {
    enum SongsDownloaderResult {
        case success([Song])
        case failure(Error?)
    }
}

//
//  ImageDownloader.swift
//  songlist
//
//  Created by Alexey Tsyss on 07/10/2017.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//
//  Use this class to cache images downloaded from the Internet.
//  It has a 'shared' instance, you can use across the application, or you're free to create your own instance of the class with it's own cache object.
//  The class uses the NSCache to cache images and HttpClientProtocol to request the backend. The instance of the HttpClientProtocol can be passed via init method.

import UIKit

class ImageDownloader {
    private let cache = NSCache<AnyObject, UIImage>()
    private var httpClient: HttpClientProtocol!
    
    static let shared = ImageDownloader()

    init(httpClient: HttpClientProtocol = HttpClient.default) {
        self.httpClient = httpClient
    }
    
    func getImage(url: URL, completion: ((UIImage?) -> Void)?) {
        if let imageFromCache = imageFromCache(url: url) {
            completion?(imageFromCache)
        } else {
            httpClient.request(url: url) { [weak self] result in
                if case HttpClientResult.success(let data) = result, let image = UIImage(data: data) {
                    self?.cache.setObject(image, forKey: url as AnyObject)
                    completion?(image)
                } else {
                    completion?(nil)
                }
            }
        }
    }
    
    func imageFromCache(url: URL) -> UIImage? {
        return cache.object(forKey: url as AnyObject)
    }
}

//
//  Player.swift
//  songlist
//
//  Created by Alexey Tsyss on 09/10/2017.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//
//  This class allows you to play a playlist of Playable items, provideing the basic capabilities of regular player, like
//  next, forward and so fotrh. The player support caching all played items using the PlayerCache class.

import Foundation
import AVFoundation
import AVKit

protocol Playable {
    var audioLink: URL { get }
}

enum PlayerState {
    case finished(Int)
    case started(Int)
}

typealias PlayerStateObserver = (PlayerState) -> Void
typealias PlayerProgressObserver = (Double, Double) -> Void

class Player: NSObject {
    
    private(set) var queuePlayer = AVQueuePlayer()
    private var playerItems: [AVPlayerItem] = []
    private var items: [Playable] = []
    
    private let playerCache = PlayerCache()
    
    private var isObserving = false
    private var firstItemIndex = 0
    private(set) var currentItemIndex = -1
    private static let rewindThreshold = 3
    
    private var observers: [PlayerStateObserver] = []
    private var progressObservers: [PlayerProgressObserver] = []
    
    private var timeObserver: Any?
    
    private struct ObserverContexts {
        static var currentItem = 0
    }
    
    init(items: [Playable]) {
        self.items = items
    }
    
    // MARK: - Public
    var isPlaying: Bool {
        return queuePlayer.isPlaying
    }
    
    func pause() {
        queuePlayer.pause()
        observers.forEach { $0(.finished(currentItemIndex)) }
    }
    
    func play() {
        queuePlayer.play()
        observers.forEach { $0(.started(currentItemIndex)) }
    }
    
    func togglePlaying() {
        if isPlaying {
            pause()
        } else {
            play()
        }
    }
    
    func next() {
        guard queuePlayer.items().count > 1 else {
            return
        }
        
        queuePlayer.advanceToNextItem()
    }
    
    func prev() {
        guard let currentItem = queuePlayer.currentItem else {
            return
        }
        
        let currentTimePlaying = Int(currentItem.currentTime().seconds)
        
        if currentTimePlaying < Player.rewindThreshold && currentItemIndex > 0 {
            playFrom(index: currentItemIndex - 1)
        } else {
            queuePlayer.seek(to: kCMTimeZero)
        }
    }
    
    func seekTo(position: Float) {
        guard let currentItem = queuePlayer.currentItem else {
            return
        }
        
        let time = currentItem.duration.seconds * Double(position)
        let cmTime = CMTimeMakeWithSeconds(time, 1)
                
        queuePlayer.seek(to: cmTime)
    }
    
    func playFrom(index: Int) {
        guard index >= 0 && index < items.count else {
            return
        }
        
        firstItemIndex = index
        
        queuePlayer.removeAllItems()
        removeObservers()
        
        playerItems = Array(index...items.count - 1)
            .map { playerCache.playerItem(for: items[$0].audioLink) }
            .flatMap { $0 }
        
        queuePlayer = AVQueuePlayer(items: playerItems)
        
        addObservers()
        queuePlayer.play()
    }
    
    func addObserver(observer: @escaping PlayerStateObserver) {
        observers.append(observer)
    }
    
    func addProgressObserver(observer: @escaping PlayerProgressObserver) {
        progressObservers.append(observer)
    }
    
    // MARK: - Private
    private func addObservers() {
        queuePlayer.addObserver(self,
                                forKeyPath: #keyPath(AVQueuePlayer.currentItem),
                                options: [.old, .new, .initial],
                                context: &ObserverContexts.currentItem)
        
        timeObserver = queuePlayer.addPeriodicTimeObserver(forInterval: CMTimeMake(1, 2), queue: .main) { [weak self] time in
            self?.handleCurrentItemProgress(time: time.seconds)
        }
        
        isObserving = true
    }
    
    private func removeObservers() {
        guard isObserving else {
            return
        }
        
        queuePlayer.removeObserver(self, forKeyPath: #keyPath(AVQueuePlayer.currentItem), context: &ObserverContexts.currentItem)
        
        if let timeObserver = timeObserver {
            queuePlayer.removeTimeObserver(timeObserver)
        }
    }
    
    private func handleCurrentItemProgress(time: Double) {
        guard let currentItem = queuePlayer.currentItem else {
            return
        }
        
        let duration = currentItem.duration.seconds   
        progressObservers.forEach { $0(time, duration) }
    }
    
    // MARK: - KVO
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &ObserverContexts.currentItem {
            if let change = change, change[.oldKey] != nil {
                playerCache.cacheItem(for: items[currentItemIndex].audioLink)
                observers.forEach { $0(.finished(currentItemIndex)) }
            }
            
            if let currentItem = queuePlayer.currentItem, let index = playerItems.index(of: currentItem) {
                currentItemIndex = Int(index) + firstItemIndex
                observers.forEach { $0(.started(currentItemIndex)) }
            }            
        } else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}

extension AVPlayer {
    var isPlaying: Bool {
        return rate != 0 && error == nil
    }
}

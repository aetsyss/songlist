//
//  HttpClient.swift
//  songlist
//
//  Created by Alex Tsyss on 07.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//
//  Use this class for simple straightforward requests to a remote server.
//  No cache, no state. Also the default instance is available.
//  The class is under the HttpClientProtocol, which allows havinv multiple implementations.

import Foundation

class HttpClient: HttpClientProtocol {
    
    static let `default` = HttpClient()

    func request(url: URL, completion: ((HttpClientResult) -> Void)?) {
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let data = data {
                    completion?(HttpClientResult.success(data))
                } else {
                    completion?(HttpClientResult.failure(error))
                }
            }
        }
        
        task.resume()
    }
}

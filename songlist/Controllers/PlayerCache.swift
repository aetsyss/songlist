//
//  PlayerCache.swift
//  songlist
//
//  Created by Alexey Tsyss on 16/10/2017.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//
//  This class servers as a cache for Player class. It provides the basic cache functions.
//  It has two dependencies - HttpClientProtocol and FileManager wich can be set in the init method.
//  The class is responsible for creating AVPlayerItems instance either from remote URL, or from local file path.
//  When it loads an item from the backend it caches it in the local storage for future use.

import Foundation
import AVFoundation

class PlayerCache {
    private var httClient: HttpClientProtocol!
    private var fileManager: FileManager!
    
    init(httpClient: HttpClientProtocol = HttpClient.default, fileManager: FileManager = .default) {
        self.httClient = httpClient
        self.fileManager = fileManager
    }
    
    func cacheItem(for url: URL) {
        guard let localUrl = localURL(for: url), fileManager.fileExists(atPath: localUrl.path) == false else {
            return
        }
        
        httClient.request(url: url) { result in
            switch result {
            case .success(let data):
                do {
                    try data.write(to: localUrl)
                    print("item \(url) saved to \(localUrl)")
                } catch { }
                
            default:()
            }
        }
    }
    
    func playerItem(for url: URL) -> AVPlayerItem? {
        guard let localUrl = localURL(for: url) else {
            return nil
        }
        
        if fileManager.fileExists(atPath: localUrl.path) {
            return AVPlayerItem(url: localUrl)
        } else {
            return AVPlayerItem(url: url)
        }
    }
    
    private func localURL(for url: URL) -> URL? {
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            return nil
        }
        
        let localUrl = dir.appendingPathComponent(url.lastPathComponent)
        return localUrl
    }
}

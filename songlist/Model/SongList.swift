//
//  SongsList.swift
//  songlist
//
//  Created by Alex Tsyss on 07.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import Foundation

struct SongList: Codable {
    let songs: [Song]
    
    enum CodingKeys: String, CodingKey {
        case songs = "data"
    }
}

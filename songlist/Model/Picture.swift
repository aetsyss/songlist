//
//  Picture.swift
//  songlist
//
//  Created by Alex Tsyss on 07.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import Foundation

struct Picture: Codable {
    let s: URL
    let m: URL
    let l: URL
    let xs: URL
    let url: URL
}

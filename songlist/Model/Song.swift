//
//  Song.swift
//  songlist
//
//  Created by Alex Tsyss on 07.10.17.
//  Copyright © 2017 Alex Tsyss. All rights reserved.
//

import Foundation

struct Song: Playable, Codable {
    struct Author: Codable {
        let name: String
        let picture: Picture
    }
    
    let author: Author
    let createdOn: Date
    let modifiedOn: Date
    let picture: Picture
    let audioLink: URL
    let id: String
    let name: String
}

# README #

iOS Tech Task for the BandLab.

### Structure ###
The project structure is divided into three parts: 
* Controllers - means utility classes which handle the business logic;
* Model - the application data model layer;
* Stories - contains all UIView and UIViewController related stuff. Typically each story corresponds to one application screen;
* Apart from that there are AppDelegate and Storyboards files that go separately.

### Controllers ###
* SongsDownloader is responsible for download json data from the backend and parse it into the data model structures. It has one dependency which is under the HttpClientProtocol and can be set in the init method;
* ImageDownloader is responsible for downloading and caching images. In the current (simple) implementation the cashe is not stored on the disc, but just in the app memory;
* HttpClient class implements the HttpClientProtocol and is responsible for simple requests to the backend. It has a default singleton.
* Player is the heart of the app and provides the basic interface of the queue player with caching feature. Create the instance of the class passing the array of the Playable items, add observers for keeping track of playback progress and you're done :)
* PlayerCache is a simple cache for the Player. It downloads an item from the backend and stores is locally. It has two dependencies - HttpClientProtocol and FileManager which can be set in the init method.

### Model ###
* SongList is a structure representing the root object of the backend response and keeps the array of songs.
* Picture is a structure representing the kinds of pitcure
* Song is a structure represinting all the data relaited to the song.

### Stories ###
* SongsList is the main app story consisted of a UITableView with song items and Bottom Player. In favour of simplicity I decided to put the "Interactor" and "Presenter" stuff inside the corresponding UIViewController (SongsListViewController). But ideally we could use a dedicated Interactor and Presenter here. I only moved the UITableViewCell logic into separate class (SongTableViewCell). Also the routing is handled inside the UIViewController as well. The story creates the Player and the SongsDownloader instances.
* Full Player is a full screen representation of the Bottom Player. It uses the same instance of the player that the SongList does, so they could react on the player events synchronously. Basically the view controller (PlayerViewController) just handles the user interactions and transfers them to the player instance. It also observers the player events and updates the UI respectively.